package com.lsgp.controller;

import com.lsgp.model.Triangle;

public class TriangleController {
	
	public boolean  validateData(Triangle t)
	{
		//(ab<=bc+ac)&&(bc<=ab+ac)&&(ac<=ab+bc)
		if(( t.getAb() < t.getBc() + t.getAc() ) && (t.getBc() < t.getAb() + t.getAc()) && (t.getAc() < t.getAb() + t.getBc()))
		{
			return true;
		}
		else {
			return false;
		}
		
	}

	public String checkTriangleType(Triangle t)
	{
		if(t.getAb() == t.getBc() && t.getBc() == t.getAc() && t.getAb() == t.getAc() )
			return "Eqilatral Triangle";
		else if(( t.getAb() == t.getBc() && t.getAb() != t.getAc()) || ( t.getAb() == t.getAc() && t.getAb() != t.getBc()) ||( t.getAc() == t.getBc() && t.getAb() != t.getBc()))
			return "Isosceles Triangle";
		else{
			return "Scalene triangle ";
		}
	}
	
	public void calCoordinates( Triangle t)
	{
	
		float ab = t.getAb(),bc = t.getBc(),ac = t.getAc();
    	
    	float x1 = 0;
    	float y1 = 0;
    	float x2 = x1;
    	float y2 = y1 + ab;
    	
    	float y3 = (((ab*ab + ac * ac) - ( bc*bc))/ (2 * ab));
    	float x3 = ((float) Math.sqrt(ac*ac - y3*y3));
    	
    	t.setX1(x1);
		t.setY1(x2);
		t.setX2(x2);
		t.setY2(y2);
		t.setX3(x3);
		t.setY3(y3);
    	
    }
}
