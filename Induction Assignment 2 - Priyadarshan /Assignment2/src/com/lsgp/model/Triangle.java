package com.lsgp.model;

public class Triangle {

	float ab;
	float bc;
	float ac;
	
	float x1;
	float y1;
	float x2;
	float y2;
	float x3;
	float y3;
	public float getAb() {
		return ab;
	}
	public void setAb(float ab) {
		this.ab = ab;
	}
	public float getBc() {
		return bc;
	}
	public void setBc(float bc) {
		this.bc = bc;
	}
	public float getAc() {
		return ac;
	}
	public void setAc(float ac) {
		this.ac = ac;
	}
	public float getX1() {
		return x1;
	}
	public void setX1(float x1) {
		this.x1 = x1;
	}
	public float getY1() {
		return y1;
	}
	public void setY1(float y1) {
		this.y1 = y1;
	}
	public float getX2() {
		return x2;
	}
	public void setX2(float x2) {
		this.x2 = x2;
	}
	public float getY2() {
		return y2;
	}
	public void setY2(float y2) {
		this.y2 = y2;
	}
	public float getX3() {
		return x3;
	}
	public void setX3(float x3) {
		this.x3 = x3;
	}
	public float getY3() {
		return y3;
	}
	public void setY3(float y3) {
		this.y3 = y3;
	}
	
}
