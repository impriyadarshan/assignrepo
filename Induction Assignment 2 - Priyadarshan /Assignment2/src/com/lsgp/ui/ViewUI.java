package com.lsgp.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.lsgp.controller.TriangleController;
import com.lsgp.model.Triangle;

public class ViewUI {


	TriangleController tc = new TriangleController();
	Triangle t = new Triangle();
	JFrame f = new JFrame("Triangle Problem"); 
	JLabel head = new JLabel("Enter Sides of Triangle");		
	JLabel txtAB = new JLabel("AB : ");		
	JTextField tfAB = new JTextField();
	JLabel txtBC = new JLabel("BC : ");
	JTextField tfBC = new JTextField();
	JLabel txtAC = new JLabel("AC : ");
	JTextField tfAC = new JTextField();
	JButton find = new JButton("Find");   
	
	JLabel triType = new JLabel("Type of triangle");
	
	JPanel triPan ;
	
	public ViewUI(){
	
		head.setBounds(200, 20, 500, 15);
		txtAB.setBounds(50, 55, 50, 15);
		tfAB.setBounds(100, 50, 50, 25);
		txtBC.setBounds(200, 55, 50, 15);
		tfBC.setBounds(250, 50, 50, 25);
		txtAC.setBounds(350, 55, 50, 15);
		tfAC.setBounds(400, 50, 50, 25);
		find.setBounds(200,100,100, 30);    
		
		triType.setBounds(200, 150, 500, 15);
		triType.setVisible(false);
		
		f.add(head);
		f.add(txtAB);
		f.add(tfAB);
		f.add(txtBC);
		f.add(tfBC);
		f.add(txtAC);
		f.add(tfAC);
		f.add(find);    
		f.add(triType);
		f.setSize(600,700);    
		f.setLayout(null);    
		f.setVisible(true);    
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
		
		find.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				getData();
				if(tc.validateData(t))
				{
					triType.setText(tc.checkTriangleType(t));
					tc.calCoordinates(t);
					drawTriangle();
					
				}
				else{
					triType.setText("Plz Enter Valid Input");
				}
				
				triType.setVisible(true);
			}
		});
	}	
	
	public void getData()
	{
		try{
			t.setAb(Float.parseFloat(tfAB.getText()));
			t.setBc(Float.parseFloat(tfBC.getText()));
			t.setAc(Float.parseFloat(tfAC.getText()));
		}catch (NumberFormatException e) {
			triType.setText("Plz Enter Valid Input");
			triPan.setVisible(false);
		}
	}
	
	@SuppressWarnings("serial")
	public void drawTriangle()
	{
		JLabel lblA = new JLabel("A");
		JLabel lblB = new JLabel("B");
		JLabel lblC = new JLabel("C");
		
		triPan = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D) g;
			    g2d.setPaint(Color.red);
			 
			    g2d.draw(new Line2D.Double(t.getX1()+50,t.getY1()+50,t.getX2()+50,t.getY2()+50 )); //ab
				g2d.draw(new Line2D.Double(t.getX2()+50,t.getY2()+50,t.getX3()+50,t.getY3()+50)); //bc
				g2d.draw(new Line2D.Double(t.getX1()+50,t.getY1()+50,t.getX3()+50,t.getY3()+50 )); //ac
			    	
				
				lblA.setBounds(Math.round(t.getX1()+40),Math.round(t.getY1()+40), 10, 10);
				lblB.setBounds(Math.round(t.getX2()+40),Math.round(t.getY2()+45), 10, 10);
				lblC.setBounds(Math.round(t.getX3()+53),Math.round(t.getY3()+45), 10, 10);
				
				triPan.add(lblA);
				triPan.add(lblB);
				triPan.add(lblC);
				
			}
			@Override
			public Dimension getPreferredSize() {
				return super.getPreferredSize();
			}
		};
		
			
		triPan.setBounds(100, 200, 600, 600);
		//triPan.setBackground(Color.lightGray);
		f.add(triPan);
		triPan.repaint();
		triPan.setVisible(true);
	}
	
	
	
}
