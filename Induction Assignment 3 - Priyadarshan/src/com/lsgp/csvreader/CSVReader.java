package com.lsgp.csvreader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.lsgp.model.VehicleData;

public class CSVReader {

	
	
	
	
	public static ArrayList<VehicleData> readCSV(String csvFile){
		
		ArrayList<VehicleData> dataList = new ArrayList<>();
		
		//String csvFile =/* "/home/pmeshram/java/Sample.csv";// */ "/home/pmeshram/Downloads/VData.csv";
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) 
        {

            while ((line = br.readLine()) != null) 
            {
            	
            	try {
            		// use comma as separator
                    String[] VDataPS = line.split(cvsSplitBy);

                    dataList.add(new VehicleData( Long.parseLong(VDataPS[0]), Long.parseLong(VDataPS[1]), Long.parseLong(VDataPS[2]), Long.parseLong(VDataPS[3])));
	
				} catch (Exception e) {
					System.out.println("Error in record");
				}
                         
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        //System.out.println(dataList.toString());
        return dataList;
       
	}
}
