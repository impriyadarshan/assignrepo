package com.lsgp.csvwriter;


import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import com.lsgp.model.DrivingSession;

public class CSVWriter {

	 //private static final char DEFAULT_SEPARATOR = ',';
	  
	 public static FileWriter writer;
	
	public static void writeCSV(String path,ArrayList<DrivingSession> dsList){
		
		try {
				writer = new FileWriter(path);
			
			 	ArrayList<String> header = new ArrayList<>();
		        header.add("Driving Session");
		        header.add("Start Time");
		        header.add("End Time");
		        header.add("Distance");
		        CSVWriter.writeLine(writer, header); 
		        
		        
		        for (DrivingSession data : dsList) {
		        	ArrayList<String> con = new ArrayList<>();
		        	if (data.getDrivingStatus() == 1) {
		        		con.add("Off Driving");
					}else {
						con.add("On Driving");
					}
		        	
		        	con.add(Long.toString(data.getStartTime()));
		        	con.add(Long.toString(data.getEndTime()));
		        	con.add(Long.toString(data.getDistance()));
		        	CSVWriter.writeLine(writer, con); 
				}
		        
		        
		        writer.flush();
		        writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
		
	public static void writeLine(Writer w, ArrayList<String> values) throws IOException {
		 	
		 //StringBuilder sb = new StringBuilder();
		 String t = values.get(0) + ","+values.get(1)+","+values.get(2)+","+values.get(3) + "\n";
		 //sb.append(values.get(0)).append(",").append(values.get(1)).append(",").append(values.get(2)).append(",").append(values.get(3));
		 //sb.append("\n");
		 w.append(t);

		 
		 
	    }
	 
	 
}
