package com.lsgp.sessioncal;

import java.util.ArrayList;

import com.lsgp.csvwriter.CSVWriter;
import com.lsgp.model.DrivingSession;
import com.lsgp.model.VehicleData;

public class SessionCalculator {
	public static void calDrivingSession(int ds,long st,long et,long dis){
		if(ds == 0)
		{
			System.out.println("...OFF Driving - "+"ST : " + st + " ET :" + et + " Distance :" + dis);
			
		}
		else {
			System.out.println("...On Driving - "+"ST : " + st + " ET :" + et+ " Distance :" + dis);
	
		}
	}
	public static void sessionCal(ArrayList<VehicleData> dataList,String outputCSVPath,int X1,int Y1,int X2,int Y2){
		ArrayList<DrivingSession> dsList = new ArrayList<>();
		
		
		int x1 = X1;
		int y1 = Y1;
		int x2 = X2;
		int y2 = Y2;
		int i = 0;
        int drivingS = 0; //driving Status
        int st = 0; //start time
        int et = 0; //end time
        int lCount = 0; //lower count below x2
        int uCount = 0; //upper count above x1
        while (i <= dataList.size() - 1) {
			
        	if(drivingS == 0){
        		if (i == dataList.size() -1) {
        			et = i;
        			calDrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(),dataList.get(et).getOdometer() - dataList.get(st).getOdometer());
					dsList.add(new DrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(), dataList.get(et).getOdometer() - dataList.get(st).getOdometer()));
					
				} 
        		//System.out.println("0");
        		if(dataList.get(i).getvSpeed() >= x1 && uCount == 0 && i != 0){
        			et = i -1  ;
        			i++;uCount++;
				}
        		else if (dataList.get(i).getvSpeed() >= x1 && uCount < y1) {
					i++;uCount++;lCount = 0;
				}
        		else if (dataList.get(i).getvSpeed() >= 10 && uCount >= y1 ) {
        			if (et != 0) {
        				calDrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(),dataList.get(et).getOdometer() - dataList.get(st).getOdometer());
    					dsList.add(new DrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(), dataList.get(et).getOdometer() - dataList.get(st).getOdometer()));
    					st = et+1;	
					}
        			
					drivingS = 1;
					i++;uCount++;lCount = 0;
				}else if (dataList.get(i).getvSpeed() < x1  ) {
					i++;lCount = 0;uCount = 0;
				}
        	}
        	else {
        		if (i == dataList.size()-1) {
        			et = i;
        			calDrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(),dataList.get(et).getOdometer() - dataList.get(st).getOdometer());
					dsList.add(new DrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(), dataList.get(et).getOdometer() - dataList.get(st).getOdometer()));
					
				} 
        		//System.out.println("1");
        		if(dataList.get(i).getvSpeed() >= x1){
        			i++;uCount++;
        		}else if (dataList.get(i).getvSpeed() < x1 && dataList.get(i).getvSpeed() >= x2) {
					 i++; lCount = 0;uCount = 0;
				}else if (dataList.get(i).getvSpeed() < x2 && lCount == 0 && i != 0) {
					et = i - 1;
					i++;lCount++;
				}else if (dataList.get(i).getvSpeed() < x2 && lCount < y2) {
					i++;lCount++;
				}else if (dataList.get(i).getvSpeed() < x2 && lCount >= y2) {
					calDrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(),dataList.get(et).getOdometer() - dataList.get(st).getOdometer());
					dsList.add(new DrivingSession(drivingS, dataList.get(st).getTime(), dataList.get(et).getTime(), dataList.get(et).getOdometer() - dataList.get(st).getOdometer()));
					st = et+1;
					i++; lCount++; uCount = 0;
					drivingS = 0;

				}
			}
        	
		}

        //String csvOutput = "/home/pmeshram/java/DSCsv.csv";
        CSVWriter.writeCSV(outputCSVPath + "DSCsv.csv", dsList);

	}
}
