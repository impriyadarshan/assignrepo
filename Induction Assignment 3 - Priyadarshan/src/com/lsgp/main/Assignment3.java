package com.lsgp.main;



import java.awt.Component;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import com.lsgp.csvreader.CSVReader;
import com.lsgp.model.VehicleData;
import com.lsgp.sessioncal.SessionCalculator;
import com.lsgp.utils.Utils;

public class Assignment3 {
	
	
	

		public static void main(String[] args) {
		
			
			
			//String csvFile =/* "/home/pmeshram/java/Sample.csv";// */ "/home/pmeshram/Downloads/VData.csv";
			
//			System.out.print("Where :");
//			System.out.println("X1 = 10");
//			System.out.println("y1 = 5");
//			System.out.println("x2 = 5");
//			System.out.println("y2 = 5");
//			
			try {
				
				
				System.out.println("Plz Enter correct file path of CSV file");
				System.out.println("Example - /home/pmeshram/java/VData.csv");
				String inputCSV = Utils.sc.next();
				ArrayList<VehicleData> dataList = CSVReader.readCSV( inputCSV );
			
				System.out.println("Enter x1 : ");
				int x1 =	Utils.sc.nextInt();
				System.out.println("Enter y1 : ");
				int y1 =	Utils.sc.nextInt();
				System.out.println("Enter x2 : ");
				int x2 =	Utils.sc.nextInt();
				System.out.println("Enter y2 : ");
				int y2 =	Utils.sc.nextInt();
				
				
				System.out.println("Plz Enter destination path to save CSV Output file");

				System.out.println("Example - /home/pmeshram/java/");
				
				String outputCSVPath =  Utils.sc.next();
		        
		        SessionCalculator.sessionCal(dataList,outputCSVPath,x1,y1,x2,y2);
	
			} catch (Exception e) {
				//e.printStackTrace();
				String bad_input = Utils.sc.next();
				System.out.println("Bad input: " + bad_input + " Please Enter a Valid Input");
			}finally {
				Utils.sc.close();
			}
				         
	      
		/*	String filename = File.separator+"tmp";
			JFileChooser fc = new JFileChooser(new File(filename));

			Component frame = null;
			// Show open dialog; this method does not return until the dialog is closed
			fc.showOpenDialog(frame);
			File selFile = fc.getSelectedFile();

			// Show save dialog; this method does not return until the dialog is closed
			fc.showSaveDialog(frame);
			selFile = fc.getSelectedFile();
	      	
		System.out.println(	selFile.getPath());
	        */
	        	        
	        
	        
	}

}
