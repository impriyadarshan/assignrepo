package com.lsgp.model;

public class VehicleData {

	long time;
	long vSpeed;
	long eSpeed;
	long odometer;
	
	
	public VehicleData(long time, long vSpeed, long eSpeed, long odometer) {
		super();
		this.time = time;
		this.vSpeed = vSpeed;
		this.eSpeed = eSpeed;
		this.odometer = odometer;
	}


	@Override
	public String toString() {
		return "VehicleData [time=" + time + ", vSpeed=" + vSpeed + ", eSpeed=" + eSpeed + ", odometer=" + odometer
				+ "]";
	}


	public long getTime() {
		return time;
	}


	public void setTime(long time) {
		this.time = time;
	}


	public long getvSpeed() {
		return vSpeed;
	}


	public void setvSpeed(long vSpeed) {
		this.vSpeed = vSpeed;
	}


	public long geteSpeed() {
		return eSpeed;
	}


	public void seteSpeed(long eSpeed) {
		this.eSpeed = eSpeed;
	}


	public long getOdometer() {
		return odometer;
	}


	public void setOdometer(long odometer) {
		this.odometer = odometer;
	}
	
	
	
	
}
