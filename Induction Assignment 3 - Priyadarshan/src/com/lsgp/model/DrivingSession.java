package com.lsgp.model;

public class DrivingSession {
	
	
	int drivingStatus;
	long startTime;
	long endTime;
	long distance;
	
	
	public DrivingSession(int drivingStatus, long startTime, long endTime, long distance) {
		super();
		this.drivingStatus = drivingStatus;
		this.startTime = startTime;
		this.endTime = endTime;
		this.distance = distance;
	}


	public int getDrivingStatus() {
		return drivingStatus;
	}


	public void setDrivingStatus(int drivingStatus) {
		this.drivingStatus = drivingStatus;
	}


	public long getStartTime() {
		return startTime;
	}


	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}


	public long getEndTime() {
		return endTime;
	}


	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}


	public long getDistance() {
		return distance;
	}


	public void setDistance(int distance) {
		this.distance = distance;
	}


	@Override
	public String toString() {
		return "DrivingSession [drivingStatus=" + drivingStatus + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", distance=" + distance + "]";
	}
	
	
	
	

}
