package com.lsgp.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ExpandableTest.class, MaxNumberTest.class, MinNumberTest.class })
public class AllTestsSuite {

}
