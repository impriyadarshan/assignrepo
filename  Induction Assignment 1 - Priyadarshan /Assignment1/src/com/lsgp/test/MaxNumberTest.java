package com.lsgp.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.lsgp.MyArrayList.MyArrayList;

public class MaxNumberTest {

	@Test
	public void test() {
		int testarr[] = {23,34,45,12,56,78,98,65,54};
		MyArrayList arr = new MyArrayList();
		
		for(int i = 0;i<testarr.length;i++){
			
			arr.add(testarr[i]);
		}
		assertEquals(98, arr.getMaxValue());
	}

}
