package com.lsgp.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.lsgp.MyArrayList.MyArrayList;

public class ExpandableTest {

	@Test
	public void test() {
		int testarr[] = {23,34,45,12,56,78,98,65,54};
		MyArrayList arr = new MyArrayList();
		int count = 0;
		for(int i = 0;i<testarr.length;i++){
			count++;
			arr.add(testarr[i]);
		}
		assertEquals(count, testarr.length);
	}

}
