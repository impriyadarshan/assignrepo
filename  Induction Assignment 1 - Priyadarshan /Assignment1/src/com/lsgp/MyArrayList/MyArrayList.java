package com.lsgp.MyArrayList;

public class MyArrayList {
private static final int SIZE_FACTOR=5;
	
	private Integer data[];
	
	private int index;
	
	private int size;
	
	public MyArrayList(){
		this.data=new Integer[SIZE_FACTOR];
		this.size=SIZE_FACTOR;
	}
	/* function to add number in an array */
	public void add(Integer obj){	
		if(this.index==this.size-1){
		
			increaseSizeAndReallocate();
		}
		data[this.index]=obj;
		this.index++;
		
	}
	/*function to increase size of array in case array is full*/
	private void increaseSizeAndReallocate() {	
		this.size=this.size+SIZE_FACTOR;
		Integer newData[]=new Integer[this.size];
		for(int i=0; i<data.length;i++){
			newData[i]=data[i];
		}
		this.data=newData;
	}
	/*function to get number in an array at index i*/
	public Integer get(int i) throws Exception{	
		if(i>this.index-1){
			throw new Exception("ArrayIndexOutOfBound");
		}
		if(i<0){
			throw new Exception("Negative Value");
		}
		return this.data[i];
		
	}
	/*function to remove number in an array at index i*/
	public void remove(int i) throws Exception{	
		if(i>this.index-1){
			throw new Exception("ArrayIndexOutOfBound");
		}
		if(i<0){
			throw new Exception("Negative Value");
		}
		for(int x = i; x < this.data.length-1;x++){
			data[x]=data[x+1];
		}
		this.index--;
	}
	
	/*function to get maximum value in an array*/
	public int getMaxValue(){	
		int maxValue = this.data[0];
		for(int i=1;i < data.length;i++){
			if(this.data[i] != null)
				if(this.data[i] > maxValue){
					maxValue = (int)this.data[i];
				}
		}
		return maxValue;
	}
	/*function to get minimum value in an array*/
	public int getMinValue(){	
		Integer minValue = this.data[0];
		System.out.println(this.data[0]);
		
		for(int i=1;i < data.length;i++){
			
			if(this.data[i] != null)
				if(this.data[i] < minValue){
					minValue = (int)this.data[i];
				}
		}
		return minValue;
	}
	/*function to print numbers in an array*/
	public void printArray(){	
		System.out.println();
		for(int i=0; i < this.data.length-1;i++){
			if(this.data[i] != null)
				
				System.out.print(" " +this.data[i]);
		}
		System.out.println();
	}


}
