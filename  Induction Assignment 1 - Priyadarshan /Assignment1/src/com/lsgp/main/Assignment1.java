package com.lsgp.main;

import java.util.InputMismatchException;


import com.lsgp.MyArrayList.MyArrayList;
import com.lsgp.utils.Utils;

public class Assignment1 {

	public static void main(String[] args) {

		System.out.println(".....Assignment1.....");
		System.out.println("");
		MyArrayList arr = new MyArrayList();
	
		try 
		{

			while (true)
			{

				System.out.println("Enter Choice ");
				System.out.println("1. Add Number in Array");
				System.out.println("2. Rermove Number at index");
				System.out.println("3. Get Number at index");
				System.out.println("4. Get Min and Max Value of an Array");
				System.out.println("5. Print Array");
				System.out.println("6. Exit");

				try
				{
					int choice = Utils.sc.nextInt();
					switch (choice)
					{
						case 1:
							System.out.println("Enter Number : ");
							arr.add(Utils.sc.nextInt());
							break;
						case 2:
							System.out.println("Enter index : ");
							arr.remove(Utils.sc.nextInt());
							System.out.println("Number removed ");
							break;
						case 3:
							System.out.println("Enter index : ");
							int i = Utils.sc.nextInt();
							System.out.println("Number at index "+i+" is "+arr.get(i));
							break;
						case 4:
							System.out.println("Min Value = " + arr.getMinValue() + " and Max Value " + arr.getMaxValue());
							break;
						case 5:
							arr.printArray();
							break;
						case 6:
							System.exit(0);
							break;
						default:
							System.out.println("Enter a Valid Choice");
					}

				}catch (InputMismatchException e)
				{
					String bad_input = Utils.sc.next();
					System.out.println("Bad input: " + bad_input + " Please Enter a Valid Input");
					continue;
				} catch (ArrayIndexOutOfBoundsException e) {

					e.printStackTrace();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}

		} finally {
			Utils.sc.close();
		}

	}

}